package me.sharkz.ultrahomes.commands;

import com.google.common.collect.ImmutableMap;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import me.sharkz.ultrahomes.UH;
import me.sharkz.ultrahomes.homes.Home;
import me.sharkz.ultrahomes.homes.HomeManager;
import me.sharkz.ultrahomes.translations.UhEn;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 */
public class HomeCommand extends MilkaCommand {

    private final HomeManager manager;

    public HomeCommand(HomeManager manager, String description) {
        this.manager = manager;
        setDescription(description);
        setConsoleCanUse(false);
        addOptionalArg("name");
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        if (args.length == 0) {
            openInventory(UH.getPlugin(UH.class), player, 1);
            return CommandResult.COMPLETED;
        }
        Optional<Home> home = manager.getPlayerHome(player, argAsString(1));
        if (!home.isPresent()) {
            message(player, UhEn.UNKNOWN_HOME.name(), UhEn.UNKNOWN_HOME.toString(), ImmutableMap.of("%name%", argAsString(1)));
            return CommandResult.COMPLETED;
        }
        // TODO : add delay, actionbar, messages & titles
        player.teleport(home.get().getLocation());
        return CommandResult.COMPLETED;
    }
}
