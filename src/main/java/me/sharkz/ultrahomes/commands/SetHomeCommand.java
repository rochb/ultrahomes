package me.sharkz.ultrahomes.commands;

import com.google.common.collect.ImmutableMap;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import me.sharkz.ultrahomes.homes.Home;
import me.sharkz.ultrahomes.homes.HomeManager;
import me.sharkz.ultrahomes.translations.UhEn;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 */
public class SetHomeCommand extends MilkaCommand {

    private final HomeManager manager;

    public SetHomeCommand(HomeManager manager, String description) {
        this.manager = manager;
        setDescription(description);
        setConsoleCanUse(false);
        addRequiredArg("name");
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        Optional<Home> home = manager.getPlayerHome(player, argAsString(1));
        if (!home.isPresent()) {
            message(player, UhEn.UNKNOWN_HOME.name(), UhEn.UNKNOWN_HOME.toString(), ImmutableMap.of("%name%", argAsString(1)));
            return CommandResult.COMPLETED;
        }
        //TODO: check home limit & warn if it's reached
        manager.set(home.get(), player.getLocation());
        message(player, UhEn.HOME_SUCCESSFULLY_SET.name(), UhEn.HOME_SUCCESSFULLY_SET.toString(), ImmutableMap.of("%name%", argAsString(1)));
        return CommandResult.COMPLETED;
    }
}
