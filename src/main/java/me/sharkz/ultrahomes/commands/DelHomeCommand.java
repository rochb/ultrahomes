package me.sharkz.ultrahomes.commands;

import com.google.common.collect.ImmutableMap;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import me.sharkz.ultrahomes.homes.Home;
import me.sharkz.ultrahomes.homes.HomeManager;
import me.sharkz.ultrahomes.translations.UhEn;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 */
public class DelHomeCommand extends MilkaCommand {

    private final HomeManager manager;

    public DelHomeCommand(HomeManager manager, String description) {
        this.manager = manager;
        setDescription(description);
        setConsoleCanUse(false);
        addRequiredArg("name");
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        Optional<Home> home = manager.getPlayerHome(player, argAsString(1));
        if (!home.isPresent()) {
            message(player, UhEn.UNKNOWN_HOME.name(), UhEn.UNKNOWN_HOME.toString(), ImmutableMap.of("%name%", argAsString(1)));
            return CommandResult.COMPLETED;
        }
        manager.delete(home.get());
        message(player, UhEn.HOME_SUCCESSFULLY_DELETED.name(), UhEn.HOME_SUCCESSFULLY_DELETED.toString(), ImmutableMap.of("%name%", argAsString(1)));
        return CommandResult.COMPLETED;
    }
}
