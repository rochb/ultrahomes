package me.sharkz.ultrahomes.commands;

import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import me.sharkz.milkalib.commands.defaults.HelpCommand;
import me.sharkz.milkalib.commands.defaults.ReloadCommand;
import me.sharkz.milkalib.commands.defaults.VersionCommand;
import me.sharkz.ultrahomes.UH;

/**
 * @author Roch Blondiaux
 */
public class MainCommand extends MilkaCommand {

    public MainCommand(String description, UH plugin) {
        setDescription(description);

        /* Subs */
        addSubCommand(new ReloadCommand(plugin, "ultrahomes.reload"));
        addSubCommand(new VersionCommand());
        addSubCommand(new HelpCommand(plugin));
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        HelpCommand.getInstance().sendHelpMenu(sender, 1);
        return CommandResult.COMPLETED;
    }
}
