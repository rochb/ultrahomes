package me.sharkz.ultrahomes.storage;

import me.sharkz.milkalib.storage.StorageManager;
import me.sharkz.milkalib.utils.logger.MilkaLogger;
import me.sharkz.milkalib.utils.sql.Column;
import me.sharkz.milkalib.utils.sql.DataTypes;
import me.sharkz.milkalib.utils.sql.QueryBuilder;
import me.sharkz.ultrahomes.configurations.Configuration;
import me.sharkz.ultrahomes.homes.Home;
import me.sharkz.ultrahomes.icons.HIcon;
import me.sharkz.ultrahomes.icons.IconManager;
import org.bukkit.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Roch Blondiaux
 */
public class UHStorage extends StorageManager {

    private String HOMES_TABLE;

    public UHStorage(Configuration configuration) {
        super(configuration.getStorage());
    }

    @Override
    protected void createDefaultTables() {
        /* Tables' name */
        HOMES_TABLE = getTableName("homes");

        /* Homes */
        getDatabase().query(new QueryBuilder(HOMES_TABLE).createTableIfNotExists()
                .column(Column.dataType("id", DataTypes.Limit.VARCHAR, 100))
                .column(Column.dataType("name", DataTypes.Limit.VARCHAR, 100))
                .column(Column.dataType("owner", DataTypes.Limit.VARCHAR, 100))
                .column(Column.dataType("server", DataTypes.Limit.VARCHAR, 100))
                .column(Column.dataType("icon", DataTypes.Limit.VARCHAR, 100))
                .column(Column.dataType("x", DataTypes.DOUBLE))
                .column(Column.dataType("y", DataTypes.DOUBLE))
                .column(Column.dataType("z", DataTypes.DOUBLE))
                .column(Column.dataType("yaw", DataTypes.FLOAT))
                .column(Column.dataType("pitch", DataTypes.FLOAT))
                .column(Column.dataType("world", DataTypes.Limit.VARCHAR, 100))
                .primaryKey("id")
                .build());
    }

    public void insert(Home home) {
        getDatabase().asyncQuery(new QueryBuilder(HOMES_TABLE).insert()
                .insert("id", home.getId().toString())
                .insert("name", home.getName())
                .insert("icon", home.getIcon().getName())
                .insert("server", home.getServer().getName())
                .insert("owner", home.getOwner().getUniqueId().toString())
                .insert("x", home.getLocation().getX())
                .insert("y", home.getLocation().getY())
                .insert("z", home.getLocation().getZ())
                .insert("yaw", home.getLocation().getYaw())
                .insert("pitch", home.getLocation().getPitch())
                .insert("world", Objects.requireNonNull(home.getLocation().getWorld()).getName())
                .build());
    }

    public void update(Home home) {
        getDatabase().asyncQuery(new QueryBuilder(HOMES_TABLE).update()
                .set("x", home.getLocation().getX())
                .set("y", home.getLocation().getY())
                .set("z", home.getLocation().getZ())
                .set("yaw", home.getLocation().getYaw())
                .set("pitch", home.getLocation().getPitch())
                .set("world", Objects.requireNonNull(home.getLocation().getWorld()).getName())
                .set("icon", home.getIcon().getName())
                .set("server", home.getServer().getName())
                .toWhere()
                .where("id", home.getId().toString())
                .build());
    }

    public void delete(Home home) {
        getDatabase().asyncQuery(new QueryBuilder(HOMES_TABLE).delete()
                .where("id", home.getId().toString())
                .build());
    }

    public List<Home> get(IconManager manager) {
        List<Home> homes = new ArrayList<>();
        ResultSet rs = getDatabase().executeQuery(new QueryBuilder(HOMES_TABLE).select().buildAllColumns());
        try {
            while (rs.next()) {
                Optional<HIcon> icon = manager.getByName(rs.getString("icon"));
                UUID id = UUID.fromString(rs.getString("id"));
                String name = rs.getString("name");
                World world = Bukkit.getWorld(rs.getString("world"));
                Location location = new Location(world, rs.getDouble("x"),
                        rs.getDouble("y"),
                        rs.getDouble("z"),
                        rs.getFloat("yaw"),
                        rs.getFloat("pitch"));
                OfflinePlayer owner = Bukkit.getOfflinePlayer(UUID.fromString(rs.getString("owner")));
                if (name == null || world == null || !icon.isPresent()) continue;
                homes.add(new Home(id, name, owner, location, icon.get(), Bukkit.getServer()));
            }
        } catch (SQLException e) {
            MilkaLogger.error("Coudn't get homes from database: ");
            e.printStackTrace();
        }
        return homes;
    }
}
