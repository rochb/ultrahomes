package me.sharkz.ultrahomes.translations;

import me.sharkz.milkalib.translations.ITranslation;
import me.sharkz.milkalib.translations.Translation;
import me.sharkz.milkalib.translations.TranslationsManager;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public enum UhFr implements ITranslation {
    UNKNOWN_PLAYER("&c&l[!] &eImpossible de trouver ce joueur !"),
    UNKNOWN_HOME("&c&l[!] &eLe home &c&l%name%&e n'existe pas !"),
    HOME_ALREADY_EXISTS("&c&l[!] &eUn home avec ce nom existe déjà !"),
    HOME_SUCCESSFULLY_SET("&a&l[!] &7Votre home &a%name%&7 a bien été &a&mdéfini&7 !"),
    HOME_SUCCESSFULLY_DELETED("&a&l[!] &7Votre home &a%name%&7 a bien été &c&lupprimé&7 !"),
    TELEPORTATION_CANCELLED("&c&l[!] &eLa téléportation vers votre home a été annulée !"),
    ALREADY_TELEPORTING("&c&l[!] &eVous êtes déjà entrain de vous téléporter vers un autre home !"),
    LIMIT_REACHED("&c&l[!] &eVous avez atteint votre limite de home."),
    ;

    private final String content;
    private TranslationsManager manager;

    UhFr(String content) {
        this.content = content;
    }

    @Override
    public Locale getLanguage() {
        return new Locale("fr", "FR");
    }

    @Override
    public Translation get() {
        return new Translation(this, this.name(), this.content);
    }

    @Override
    public List<Translation> translations() {
        return Arrays.stream(values()).map(UhFr::get).collect(Collectors.toList());
    }

    @Override
    public void setManager(TranslationsManager manager) {
        this.manager = manager;
    }

    @Override
    public TranslationsManager getManager() {
        return manager;
    }


    @Override
    public String toString() {
        return this.get().translate();
    }
}

