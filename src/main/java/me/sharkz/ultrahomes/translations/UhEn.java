package me.sharkz.ultrahomes.translations;

import me.sharkz.milkalib.translations.ITranslation;
import me.sharkz.milkalib.translations.Translation;
import me.sharkz.milkalib.translations.TranslationsManager;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public enum UhEn implements ITranslation {
    UNKNOWN_PLAYER("&c&l[!] &eCouldn't find this player!"),
    UNKNOWN_HOME("&c&l[!] &eThe home &c&l%name%&e do not exists!"),
    HOME_ALREADY_EXISTS("&c&l[!] &eA home with this name already exists!"),
    HOME_SUCCESSFULLY_SET("&a&l[!] &7Your home &a%name%&7 has been &a&lset&7!"),
    HOME_SUCCESSFULLY_DELETED("&a&l[!] &7Your home &a%name%&7 has been &c&ldeleted&7!"),
    TELEPORTATION_CANCELLED("&c&l[!] &eYour teleportation has been cancelled!"),
    ALREADY_TELEPORTING("&c&l[!] &eYou are already teleporting to another home!"),
    LIMIT_REACHED("&c&l[!] &eYou have reach your home limit!"),
    ;

    private final String content;
    private TranslationsManager manager;

    UhEn(String content) {
        this.content = content;
    }

    @Override
    public Locale getLanguage() {
        return new Locale("en", "US");
    }

    @Override
    public Translation get() {
        return new Translation(this, this.name(), this.content);
    }

    @Override
    public List<Translation> translations() {
        return Arrays.stream(values()).map(UhEn::get).collect(Collectors.toList());
    }

    @Override
    public void setManager(TranslationsManager manager) {
        this.manager = manager;
    }

    @Override
    public TranslationsManager getManager() {
        return manager;
    }


    @Override
    public String toString() {
        return this.get().translate();
    }
}

