package me.sharkz.ultrahomes.homes;

import me.sharkz.milkalib.utils.MilkaUtils;
import me.sharkz.ultrahomes.events.HomeDeleteEvent;
import me.sharkz.ultrahomes.events.HomeSetEvent;
import me.sharkz.ultrahomes.events.HomeTeleportEvent;
import me.sharkz.ultrahomes.icons.IconManager;
import me.sharkz.ultrahomes.storage.UHStorage;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 */
public class HomeManager extends MilkaUtils {

    private final UHStorage storage;
    private final IconManager iconManager;
    private final List<Home> homes = new ArrayList<>();

    public HomeManager(UHStorage storage, IconManager iconManager) {
        this.storage = storage;
        this.iconManager = iconManager;
        this.homes.addAll(storage.get(iconManager));
    }

    /**
     * Teleport player to specified home
     *
     * @param player player to teleport
     * @param home   target home
     */
    public void teleport(Player player, Home home) {


    }

    /**
     * Add or update home to database and cache
     *
     * @param home target
     */
    public void set(Home home, Location location) {
        if (homes.contains(home)) {
            HomeSetEvent e = new HomeSetEvent(home, home.getOwner().getPlayer(), false, location);
            callEvent(e);
            if (e.isCancelled()) return;

            home.setLocation(location);
            storage.update(home);
            return;
        }

        HomeSetEvent e1 = new HomeSetEvent(home, home.getOwner().getPlayer(), true, location);
        callEvent(e1);
        if (e1.isCancelled()) return;

        homes.add(home);
        storage.insert(home);
    }

    /**
     * Delete home from cache and database
     *
     * @param home target
     */
    public void delete(Home home) {
        HomeDeleteEvent e = new HomeDeleteEvent(home, home.getOwner().getPlayer());
        callEvent(e);
        if (e.isCancelled()) return;

        homes.remove(home);
        storage.delete(home);
    }

    /**
     * Get player home by name
     *
     * @param player target
     * @param name   home's name
     * @return optional home
     */
    public Optional<Home> getPlayerHome(OfflinePlayer player, String name) {
        return getPlayerHomes(player).stream().filter(home -> home.getName().equalsIgnoreCase(name)).findFirst();
    }

    /**
     * Get player's homes
     *
     * @param player target
     * @return list of homes
     */
    public List<Home> getPlayerHomes(OfflinePlayer player) {
        return homes.stream().filter(home -> home.getOwner().equals(player)).collect(Collectors.toList());
    }

    /**
     * Get all loaded homes
     *
     * @return list of homes
     */
    public List<Home> getHomes() {
        return homes;
    }
}
