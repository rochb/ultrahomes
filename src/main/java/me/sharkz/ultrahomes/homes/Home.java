package me.sharkz.ultrahomes.homes;

import me.sharkz.ultrahomes.icons.HIcon;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;

import java.util.UUID;

/**
 * @author Roch Blondiaux
 */
public class Home {

    private final UUID id;
    private final String name;
    private final OfflinePlayer owner;
    private Location location;
    private HIcon icon;
    private Server server;

    public Home(String name, OfflinePlayer owner, Location location) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.owner = owner;
        this.server = Bukkit.getServer();
        this.location = location;
    }

    public Home(UUID id, String name, OfflinePlayer owner, Location location, HIcon icon, Server server) {
        this.id = id;
        this.name = name;
        this.owner = owner;
        this.location = location;
        this.icon = icon;
        this.server = server;
    }

    /**
     * Set home location
     *
     * @param location new home location
     */
    public void setLocation(Location location) {
        this.location = location;
        setServer(Bukkit.getServer());
    }

    /**
     * Set home icon
     *
     * @param icon new home icon
     */
    public void setIcon(HIcon icon) {
        this.icon = icon;
    }

    /**
     * Set home server
     *
     * @param server new home server
     */
    public void setServer(Server server) {
        this.server = server;
    }

    /**
     * Get home's unique id
     *
     * @return home's unique id
     */
    public UUID getId() {
        return id;
    }

    /**
     * Get home's name
     *
     * @return home's name
     */
    public String getName() {
        return name;
    }

    /**
     * Get home's owner
     *
     * @return home's owner
     */
    public OfflinePlayer getOwner() {
        return owner;
    }

    /**
     * Get home's location
     *
     * @return home's location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Get home's icon
     *
     * @return home's icon
     */
    public HIcon getIcon() {
        return icon;
    }

    /**
     * Get home's server
     *
     * @return home's server
     */
    public Server getServer() {
        return server;
    }
}
