package me.sharkz.ultrahomes.loaders;

import me.sharkz.milkalib.loaders.MilkaLoader;
import me.sharkz.milkalib.utils.xseries.XSound;
import me.sharkz.ultrahomes.notifications.*;
import org.bukkit.configuration.ConfigurationSection;

import java.util.Objects;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 */
public class NotificationLoader implements MilkaLoader<Notification> {

    @Override
    public Notification load(ConfigurationSection sec) {
        switch (sec.getName().toLowerCase()) {
            case "title":
                return new TitleNotification(sec.getString("title"), sec.getString("sub-title"));
            case "text":
                return new TextNotification(sec.getString("message"));
            case "action-bar":
                return new ActionBarNotification(sec.getString("message"));
            case "sound":
                Optional<XSound> sound = XSound.matchXSound(Objects.requireNonNull(sec.getString("sound")));
                return sound.map(xSound -> new SoundNotification(xSound, (float) sec.getDouble("pitch", 1D), (float) sec.getDouble("volume", 1D))).orElse(null);
            default:
                return null;
        }
    }

    @Override
    public void save(Notification notification, ConfigurationSection sec) {}
}
