package me.sharkz.ultrahomes.loaders;

import me.sharkz.milkalib.loaders.ItemStackLoader;
import me.sharkz.milkalib.loaders.MilkaLoader;
import me.sharkz.ultrahomes.icons.HIcon;
import org.apache.commons.lang.Validate;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

/**
 * @author Roch Blondiaux
 */
public class HIconLoader implements MilkaLoader<HIcon> {

    private final ItemStackLoader itemLoader = new ItemStackLoader();

    @Override
    public HIcon load(ConfigurationSection sec) {
        if (sec == null
                || !sec.contains("name")
                || !sec.isConfigurationSection("item")) return null;
        String name = sec.getString("name");
        String permission = sec.getString("permission");
        ItemStack icon = itemLoader.load(sec.getConfigurationSection("item"));
        if (name == null || icon == null) return null;
        return new HIcon(name, permission, icon);
    }

    @Override
    public void save(HIcon hIcon, ConfigurationSection sec) {
        Validate.notNull(hIcon);
        Validate.notNull(hIcon.getName());
        Validate.notNull(hIcon.getIcon());

        sec.set("name", hIcon.getName());
        if (hIcon.hasPermission()) sec.set("permission", hIcon.getPermission());
        itemLoader.save(hIcon.getIcon(), sec.createSection("item"));
    }
}
