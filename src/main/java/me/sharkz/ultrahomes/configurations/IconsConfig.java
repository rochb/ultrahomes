package me.sharkz.ultrahomes.configurations;

import me.sharkz.milkalib.configuration.wrapper.ConfigurableFileWrapper;
import me.sharkz.ultrahomes.UH;

import java.io.File;

/**
 * @author Roch Blondiaux
 */
public class IconsConfig extends ConfigurableFileWrapper {

    public IconsConfig(UH plugin) {
            super(new File(plugin.getDataFolder(), "icons.yml"), plugin.getClass().getClassLoader().getResource("icons.yml"));
    }
}
