package me.sharkz.ultrahomes.configurations;

import me.sharkz.milkalib.configuration.Configurable;
import me.sharkz.milkalib.configuration.wrapper.ConfigurableFileWrapper;
import me.sharkz.ultrahomes.UH;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Roch Blondiaux
 */
public class CmdConfig extends ConfigurableFileWrapper {


    @Configurable(key = "main-command")
    private ConfigurationSection mainCommand;

    @Configurable(key = "home-command")
    private ConfigurationSection homeCommand;

    @Configurable(key = "set-home-command")
    private ConfigurationSection setHomeCommand;

    @Configurable(key = "del-home-command")
    private ConfigurationSection delHomeCommand;

    public CmdConfig(UH plugin) {
        super(new File(plugin.getDataFolder(), "commands.yml"), plugin.getClass().getClassLoader().getResource("commands.yml"));
    }


    /**
     * Get command key by configuration section
     *
     * @param section target command section
     * @param key     default key
     * @return command key
     */
    public String getCmdKey(ConfigurationSection section, String key) {
        return section.getString("key", key);
    }

    /**
     * Get command description by configuration section
     *
     * @param section target command section
     * @return command description
     */
    public String getCmdDescription(ConfigurationSection section) {
        return section.getString("description");
    }

    /**
     * Get command aliases list by configuration section
     *
     * @param section target command section
     * @return command aliases array
     */
    public String[] getCmdAliases(ConfigurationSection section) {
        List<String> aliases = new ArrayList<>();
        if (section.isList("aliases") && section.getStringList("aliases").size() > 0)
            aliases.addAll(section.getStringList("aliases"));
        else if (section.isSet("aliases"))
            aliases.add(section.getString("aliases"));
        return aliases.toArray(new String[]{});
    }

    /**
     * Get main command configuration section
     *
     * @return main command configuration section
     */
    public ConfigurationSection getMainCommand() {
        return mainCommand;
    }

    /**
     * Get home command configuration section
     *
     * @return home command configuration section
     */
    public ConfigurationSection getHomeCommand() {
        return homeCommand;
    }

    /**
     * Get set home command configuration section
     *
     * @return set home command configuration section
     */
    public ConfigurationSection getSetHomeCommand() {
        return setHomeCommand;
    }

    /**
     * Get delete home command configuration section
     *
     * @return delete home command configuration section
     */
    public ConfigurationSection getDelHomeCommand() {
        return delHomeCommand;
    }
}
