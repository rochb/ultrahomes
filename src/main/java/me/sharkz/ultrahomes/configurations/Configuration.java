package me.sharkz.ultrahomes.configurations;

import me.sharkz.milkalib.configuration.Configurable;
import me.sharkz.milkalib.configuration.wrapper.ConfigurableFileWrapper;
import me.sharkz.ultrahomes.UH;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;

/**
 * @author Roch Blondiaux
 */
public class Configuration extends ConfigurableFileWrapper {

    @Configurable
    private ConfigurationSection storage;

    @Configurable(key = "check-for-update")
    private boolean checkForUpdate;

    public Configuration(UH plugin) {
        super(new File(plugin.getDataFolder(), "config.yml"), plugin.getClass().getClassLoader().getResource("config.yml"));
    }

    /**
     * Get storage configuration section
     *
     * @return storage configuration section
     */
    public ConfigurationSection getStorage() {
        return storage;
    }

    /**
     * Check if the plugin has to notify owners of each plugin updates
     *
     * @return true if it's the case
     */
    public boolean isCheckForUpdate() {
        return checkForUpdate;
    }
}
