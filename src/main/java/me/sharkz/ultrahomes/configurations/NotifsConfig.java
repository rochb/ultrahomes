package me.sharkz.ultrahomes.configurations;

import me.sharkz.milkalib.configuration.wrapper.ConfigurableFileWrapper;
import me.sharkz.ultrahomes.UH;

import java.io.File;

/**
 * @author Roch Blondiaux
 */
public class NotifsConfig extends ConfigurableFileWrapper {

    public NotifsConfig(UH plugin) {
        super(new File(plugin.getDataFolder(), "notifications.yml"), plugin.getClass().getClassLoader().getResource("notifications.yml"));
    }

}
