package me.sharkz.ultrahomes;

import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.utils.logger.MilkaLogger;
import me.sharkz.milkalib.utils.updates.UpdateChecker;
import me.sharkz.ultrahomes.commands.DelHomeCommand;
import me.sharkz.ultrahomes.commands.HomeCommand;
import me.sharkz.ultrahomes.commands.MainCommand;
import me.sharkz.ultrahomes.commands.SetHomeCommand;
import me.sharkz.ultrahomes.configurations.CmdConfig;
import me.sharkz.ultrahomes.configurations.Configuration;
import me.sharkz.ultrahomes.configurations.IconsConfig;
import me.sharkz.ultrahomes.configurations.NotifsConfig;
import me.sharkz.ultrahomes.homes.HomeManager;
import me.sharkz.ultrahomes.icons.IconManager;
import me.sharkz.ultrahomes.notifications.NotificationsManager;
import me.sharkz.ultrahomes.storage.UHStorage;
import me.sharkz.ultrahomes.teleportations.TeleportationManager;
import me.sharkz.ultrahomes.translations.UhEn;
import me.sharkz.ultrahomes.translations.UhFr;
import org.bstats.bukkit.Metrics;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 */
public class UH extends MilkaPlugin {

    /* Configurations */
    private Configuration config;
    private IconsConfig iconsConfig;
    private CmdConfig cmdConfig;
    private NotifsConfig notifsConfig;

    /* Storage */
    private UHStorage storage;

    /* Managers */
    private static IconManager iconManager;
    private static HomeManager homeManager;
    private TeleportationManager teleportationManager;
    private NotificationsManager notificationsManager;

    @Override
    public void onEnable() {
        /* Initialize */
        init(this);

        /* Header */
        MilkaLogger.info("&r");
        MilkaLogger.info("&r                 &a&l=====");
        MilkaLogger.info("&r                &a&l=======");
        MilkaLogger.info("&r             &a&l=============");
        MilkaLogger.info("&r           &a&l=======   =======");
        MilkaLogger.info("&r        &a&l========      =========");
        MilkaLogger.info("&r      &a&l=======           =========");
        MilkaLogger.info("&r     &a&l=======              =======");
        MilkaLogger.info("&r     &a&l==========            ======");
        MilkaLogger.info("&r     &a&l============          ======");
        MilkaLogger.info("&r     &a&l===============       ======");
        MilkaLogger.info("&r     &a&l============================");
        MilkaLogger.info("&r     &a&l============================");
        MilkaLogger.info("&r");
        MilkaLogger.info("&fDeveloped by &b&lSharkz");
        MilkaLogger.info("&b&lwww.roch-blondiaux");
        MilkaLogger.info("&r");
        MilkaLogger.info("&r");
        MilkaLogger.info("&fVersion &b&l" + getDescription().getVersion());
        MilkaLogger.info("&fWiki &b&lwww.uh.roch-blondiaux.com");
        createFolders();

        /* Dependencies */
        initDependencies();

        /* Configuration */
        initConfigurations();
        config = (Configuration) registerConfig(new Configuration(this));
        iconsConfig = (IconsConfig) registerConfig(new IconsConfig(this));
        cmdConfig = (CmdConfig) registerConfig(new CmdConfig(this));
        notifsConfig = (NotifsConfig) registerConfig(new NotifsConfig(this));

        /* Updates */
        if (config.isCheckForUpdate())
            new UpdateChecker("84855",
                    "",
                    "https://www.spigotmc.org/resources/ultrahomes-1-9-1-16-fully-customizable.84855/",
                    "",
                    this)
                    .check();

        /* Metrics */
        new Metrics(this, 9055);

        /* Translations */
        registerTranslation(UhEn.class);
        registerTranslation(UhFr.class);

        /* Storage */
        storage = new UHStorage(config);

        /* Icons */
        iconManager = new IconManager(iconsConfig);

        /* Notifications */
        notificationsManager = new NotificationsManager(notifsConfig);

        /* Homes */
        homeManager = new HomeManager(storage, iconManager);

        /* Teleportation */
        teleportationManager = new TeleportationManager();

        /* Commands */
        initCommands();
        registerCommand(cmdConfig.getCmdKey(cmdConfig.getMainCommand(), "uh"),
                new MainCommand(cmdConfig.getCmdDescription(cmdConfig.getMainCommand()), this),
                cmdConfig.getCmdAliases(cmdConfig.getMainCommand()));

        registerCommand(cmdConfig.getCmdKey(cmdConfig.getHomeCommand(), "home"),
                new HomeCommand(homeManager, cmdConfig.getCmdDescription(cmdConfig.getHomeCommand())),
                cmdConfig.getCmdAliases(cmdConfig.getHomeCommand()));

        registerCommand(cmdConfig.getCmdKey(cmdConfig.getSetHomeCommand(), "sethome"),
                new SetHomeCommand(homeManager, cmdConfig.getCmdDescription(cmdConfig.getSetHomeCommand())),
                cmdConfig.getCmdAliases(cmdConfig.getSetHomeCommand()));

        registerCommand(cmdConfig.getCmdKey(cmdConfig.getDelHomeCommand(), "delhome"),
                new DelHomeCommand(homeManager, cmdConfig.getCmdDescription(cmdConfig.getDelHomeCommand())),
                cmdConfig.getCmdAliases(cmdConfig.getDelHomeCommand()));


        /* UI */
        initInventories();

        /* Listeners */
    }

    @Override
    public void onDisable() {
        /* Storage */
        Objects.requireNonNull(storage).getDatabase().closeConnection();

        /* Thanks */
        sendThanks();
    }

    @Override
    public void onReload() {
        super.onReload();

        /* Icons */
        iconManager.load();

        /* Notifications */
        notificationsManager.load();
    }

    @Override
    protected boolean isPaid() {
        return false;
    }

    /**
     * Get configuration version
     *
     * @return main config version
     */
    @Override
    protected int getConfigurationVersion() {
        return 4;
    }

    /**
     * Get plugin prefix
     *
     * @return plugin prefix
     */
    @Override
    public String getPrefix() {
        return getConfig().getString("prefix", "&b&lUltra &9&lHomes &f»");
    }

    /**
     * Get plugin language
     *
     * @return plugin language
     */
    @Override
    public String getLanguage() {
        return getConfig().getString("language", "en_US");
    }

    /**
     * Get icons manager
     *
     * @return icons manager instance
     */
    public static IconManager getIconManager() {
        return iconManager;
    }

    /**
     * Get homes manager
     *
     * @return homes manager instance
     */
    public static HomeManager getHomeManager() {
        return homeManager;
    }

    /**
     * Get teleportation manager
     *
     * @return teleportation manager instance
     */
    public TeleportationManager getTeleportationManager() {
        return teleportationManager;
    }

    /**
     * Get notifications manager
     *
     * @return notifications manager instance
     */
    public NotificationsManager getNotificationsManager() {
        return notificationsManager;
    }
}
