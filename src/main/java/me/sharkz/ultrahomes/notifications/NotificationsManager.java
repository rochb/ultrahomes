package me.sharkz.ultrahomes.notifications;

import me.sharkz.ultrahomes.configurations.NotifsConfig;
import me.sharkz.ultrahomes.homes.Home;
import me.sharkz.ultrahomes.loaders.NotificationLoader;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 */
public class NotificationsManager {

    private final NotifsConfig config;
    private final Map<NotificationType, List<Notification>> notifs = new HashMap<>();

    public NotificationsManager(NotifsConfig config) {
        this.config = config;
        load();
    }

    /**
     * Load notifications from config
     */
    public void load(){
        notifs.clear();
        NotificationLoader loader = new NotificationLoader();
        notifs.put(NotificationType.CREATE, config.get()
                .getKeys(true)
                .stream()
                .map(s -> config.get().getConfigurationSection(s))
                .filter(Objects::nonNull)
                .map(loader::load)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));

    }

    /**
     * Play notifications related to action to the targeted player
     *
     * @param player targeted player
     * @param home   targeted home
     * @param type   action to notify
     */
    public void playNotifications(Player player, Home home, NotificationType type) {
        Map<String, String> placeholders = new HashMap<>();
        placeholders.put("%name%", home.getName());
        placeholders.put("%icon%", home.getIcon().getName());
        placeholders.put("%x%", String.valueOf(home.getLocation().getX()));
        placeholders.put("%y%", String.valueOf(home.getLocation().getY()));
        placeholders.put("%z%", String.valueOf(home.getLocation().getZ()));
        placeholders.put("%yaw%", String.valueOf(home.getLocation().getYaw()));
        placeholders.put("%pitch%", String.valueOf(home.getLocation().getPitch()));
        placeholders.put("%world%", Objects.requireNonNull(home.getLocation().getWorld()).getName());
        placeholders.put("%id%", home.getId().toString());

        getNotificationByType(type)
                .forEach(notification -> notification.play(player, placeholders));
    }

    /**
     * Get notifications by type
     *
     * @param type targeted type
     * @return list of notifications
     */
    public List<Notification> getNotificationByType(NotificationType type) {
        return notifs.get(type);
    }

    /**
     * Get all available notifications
     *
     * @return map of notifications
     */
    public Map<NotificationType, List<Notification>> getNotifs() {
        return notifs;
    }
}
