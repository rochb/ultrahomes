package me.sharkz.ultrahomes.notifications;

/**
 * @author Roch Blondiaux
 */
public enum NotificationType {
    CREATE,
    UPDATE,
    DELETE,
    TELEPORTING,
    TELEPORTED;
}
