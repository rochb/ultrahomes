package me.sharkz.ultrahomes.notifications;

import me.sharkz.milkalib.utils.xseries.ActionBar;
import org.bukkit.entity.Player;

import java.util.Map;

/**
 * @author Roch Blondiaux
 */
public class ActionBarNotification extends Notification {

    private final String message;

    public ActionBarNotification(String message) {
        this.message = message;
    }


    @Override
    public void play(Player player, Map<String, String> placeholders) {
        ActionBar.sendActionBar(player, color(format(player, message, placeholders)));
    }
}
