package me.sharkz.ultrahomes.notifications;

import org.bukkit.entity.Player;

import java.util.Map;

/**
 * @author Roch Blondiaux
 */
public class TextNotification extends Notification {

    private final String message;

    public TextNotification(String message) {
        this.message = message;
    }

    @Override
    public void play(Player player, Map<String, String> placeholders) {
        player.sendMessage(color(format(player, message, placeholders)));
    }
}
