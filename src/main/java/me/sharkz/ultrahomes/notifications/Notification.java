package me.sharkz.ultrahomes.notifications;

import me.sharkz.milkalib.utils.MilkaUtils;
import org.bukkit.entity.Player;

import java.util.Map;

/**
 * @author Roch Blondiaux
 */
public abstract class Notification extends MilkaUtils {

    /**
     * Play notification to player
     *
     * @param player       target
     * @param placeholders placeholders for text messages
     */
    public abstract void play(Player player, Map<String, String> placeholders);

}
