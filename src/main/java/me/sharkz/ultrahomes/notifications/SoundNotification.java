package me.sharkz.ultrahomes.notifications;

import me.sharkz.milkalib.utils.xseries.XSound;
import org.bukkit.entity.Player;

import java.util.Map;

/**
 * @author Roch Blondiaux
 */
public class SoundNotification extends Notification {

    private final XSound sound;
    private final float pitch;
    private final float volume;

    public SoundNotification(XSound sound, float pitch, float volume) {
        this.sound = sound;
        this.pitch = pitch;
        this.volume = volume;
    }

    @Override
    public void play(Player player, Map<String, String> placeholders) {
        sound.play(player, pitch, volume);
    }
}
