package me.sharkz.ultrahomes.notifications;

import me.sharkz.milkalib.utils.xseries.Titles;
import org.bukkit.entity.Player;

import java.util.Map;

/**
 * @author Roch Blondiaux
 */
public class TitleNotification extends Notification {

    private final String title;
    private final String subTitle;

    public TitleNotification(String title, String subTitle) {
        this.title = title;
        this.subTitle = subTitle;
    }

    @Override
    public void play(Player player, Map<String, String> placeholders) {
        Titles.sendTitle(player, color(format(player, title, placeholders)), color(format(player, subTitle, placeholders)));
    }
}
