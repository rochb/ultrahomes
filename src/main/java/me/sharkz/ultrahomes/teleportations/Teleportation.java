package me.sharkz.ultrahomes.teleportations;

import me.sharkz.ultrahomes.homes.Home;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 */
public class Teleportation {

    private final Home home;
    private final Player player;
    private final Location from;
    private int delay;
    private boolean cancelOnMove;
    private final long startDate;

    public Teleportation(Home home, Player player, Location from, boolean cancelOnMove, int delay) {
        this.home = home;
        this.player = player;
        this.from = from;
        this.startDate = System.currentTimeMillis();
        this.cancelOnMove = cancelOnMove;
        this.delay = delay;
    }

    /**
     * Get target home
     *
     * @return target home
     */
    public Home getHome() {
        return home;
    }

    /**
     * Get player to teleport
     *
     * @return player to teleport
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Get delay before teleportation
     *
     * @return tp delay
     */
    public int getDelay() {
        return delay;
    }

    /**
     * Set teleportation delay
     *
     * @param delay tp delay
     */
    public void setDelay(int delay) {
        this.delay = delay;
    }

    /**
     * @return boolean
     */
    public boolean isCancelOnMove() {
        return cancelOnMove;
    }

    /**
     * Define if the teleportation should be cancelled on player move
     *
     * @param cancelOnMove boolean
     */
    public void setCancelOnMove(boolean cancelOnMove) {
        this.cancelOnMove = cancelOnMove;
    }

    /**
     * Get start timestamp
     *
     * @return start timestamp
     */
    public long getStartDate() {
        return startDate;
    }

    /**
     * Get from location
     *
     * @return from location
     */
    public Location getFrom() {
        return from;
    }
}
