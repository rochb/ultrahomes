package me.sharkz.ultrahomes.teleportations;

import me.sharkz.ultrahomes.homes.Home;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 */
public class TeleportationManager {

    private final List<Teleportation> tps = new ArrayList<>();

    /**
     * Get current player teleportation
     *
     * @param player target player
     * @return optional teleportation
     */
    public Optional<Teleportation> getTeleportation(Player player) {
        return tps.stream().filter(teleportation -> teleportation.getPlayer().equals(player)).findFirst();
    }

    /**
     * Check if player is currently teleporting
     *
     * @param player target
     * @return true if it's the case
     */
    public boolean isTeleporting(Player player) {
        return tps.stream().anyMatch(teleportation -> teleportation.getPlayer().equals(player));
    }

    /**
     * Initialize a teleportation
     *
     * @param player       to teleport
     * @param home         targeted home
     * @param delay        delay before teleportation
     * @param cancelOnMove cancel if player move
     * @return true if player can init teleportation
     */
    public boolean teleportPlayer(Player player, Home home, int delay, boolean cancelOnMove) {
        if (isTeleporting(player)) return false;
        tps.add(new Teleportation(home, player, player.getLocation(), cancelOnMove, delay));
        return true;
    }

    /**
     * Cancel a teleportation
     *
     * @param teleportation target
     */
    public void cancelTeleportation(Teleportation teleportation) {
        tps.remove(teleportation);
    }

    /**
     * Get teleportation list
     *
     * @return list of teleportation
     */
    public List<Teleportation> getTeleportation() {
        return tps;
    }
}
