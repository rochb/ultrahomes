package me.sharkz.ultrahomes.events;

import me.sharkz.ultrahomes.homes.Home;
import me.sharkz.ultrahomes.teleportations.Teleportation;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 */
public class HomeTeleportEvent extends HomeEvent{

    private final Teleportation teleportation;
    private final Location from;
    private final Location to;

    public HomeTeleportEvent(Home home, Player player, Teleportation teleportation, Location from, Location to) {
        super(home, player);
        this.teleportation = teleportation;
        this.from = from;
        this.to = to;
    }

    public Location getFrom() {
        return from;
    }

    public Location getTo() {
        return to;
    }

    public Teleportation getTeleportation() {
        return teleportation;
    }
}
