package me.sharkz.ultrahomes.events;

import me.sharkz.ultrahomes.homes.Home;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 */
public class HomeDeleteEvent extends HomeEvent {

    public HomeDeleteEvent(Home home, Player player) {
        super(home, player);
    }

}
