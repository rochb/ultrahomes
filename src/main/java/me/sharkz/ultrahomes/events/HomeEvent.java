package me.sharkz.ultrahomes.events;

import me.sharkz.ultrahomes.homes.Home;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author Roch Blondiaux
 */
public class HomeEvent extends Event implements Cancellable {

    private static HandlerList HANDLERS = new HandlerList();
    private final Home home;
    private final Player player;
    private boolean cancelled;

    public HomeEvent(Home home, Player player) {
        this.home = home;
        this.player = player;
    }

    /**
     * Get home
     *
     * @return home
     */
    public Home getHome() {
        return home;
    }

    /**
     * Get player
     *
     * @return player
     */
    public Player getPlayer() {
        return player;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
