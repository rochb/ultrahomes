package me.sharkz.ultrahomes.events;

import me.sharkz.ultrahomes.homes.Home;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 */
public class HomeSetEvent extends HomeEvent {

    private final boolean New;
    private final Location newLocation;

    public HomeSetEvent(Home home, Player player, boolean aNew, Location newLocation) {
        super(home, player);
        New = aNew;
        this.newLocation = newLocation;
    }

    public boolean isNew() {
        return New;
    }

    public Location getNewLocation() {
        return newLocation;
    }
}
