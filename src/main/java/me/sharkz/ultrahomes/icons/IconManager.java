package me.sharkz.ultrahomes.icons;

import me.sharkz.milkalib.utils.items.SkullCreator;
import me.sharkz.ultrahomes.configurations.IconsConfig;
import me.sharkz.ultrahomes.loaders.HIconLoader;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 */
public class IconManager {

    private static final ItemStack defaultIcon;
    private final List<HIcon> icons = new ArrayList<>();
    private final IconsConfig config;

    static {
        defaultIcon = SkullCreator.itemFromBase64("head-eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNmZiMjkwYTEzZGY4ODI2N2VhNWY1ZmNmNzk2YjYxNTdmZjY0Y2NlZTVjZDM5ZDQ2OTcyNDU5MWJhYmVlZDFmNiJ9fX0=");
    }

    public IconManager(IconsConfig config) {
        this.config = config;
        load();
    }

    /**
     * Load all icons from configuration file
     */
    public void load() {
        icons.clear();
        icons.addAll(config.get().getKeys(true)
                .stream()
                .map(s -> config.get().getConfigurationSection(s))
                .filter(Objects::nonNull)
                .map(section -> new HIconLoader().load(section))
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
    }

    /**
     * Find icon by name
     *
     * @param name icon's name
     * @return optional icon
     */
    public Optional<HIcon> getByName(String name) {
        return icons.stream().filter(hIcon -> hIcon.getName().equals(name)).findFirst();
    }

    /**
     * Find icon by display item
     *
     * @param item display item
     * @return optional icon
     */
    public Optional<HIcon> getByItem(ItemStack item) {
        return icons.stream().filter(hIcon -> hIcon.getIcon().isSimilar(item)).findFirst();
    }

    /**
     * Get all loaded icons
     *
     * @return list of icons
     */
    public List<HIcon> getIcons() {
        return icons;
    }

    /**
     * Get default home icon
     *
     * @return default home icon
     */
    public static ItemStack getDefaultIcon() {
        return defaultIcon;
    }
}
