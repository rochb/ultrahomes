package me.sharkz.ultrahomes.icons;

import org.bukkit.inventory.ItemStack;

/**
 * @author Roch Blondiaux
 */
public class HIcon {

    private final String name;
    private final String permission;
    private final ItemStack icon;

    public HIcon(String name, String permission, ItemStack icon) {
        this.name = name;
        this.permission = permission;
        this.icon = icon;
    }

    /**
     * Get icon's name
     *
     * @return icon's name
     */
    public String getName() {
        return name;
    }

    /**
     * Check if icon has a permission defined
     *
     * @return true if it's the case
     */
    public boolean hasPermission() {
        return permission != null;
    }

    /**
     * Get icon's permission
     *
     * @return icon's permission
     */
    public String getPermission() {
        return permission;
    }

    /**
     * Get icon's display item
     *
     * @return icon's item
     */
    public ItemStack getIcon() {
        return icon;
    }
}
