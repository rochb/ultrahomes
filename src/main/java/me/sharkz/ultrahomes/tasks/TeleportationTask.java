package me.sharkz.ultrahomes.tasks;

import me.sharkz.ultrahomes.events.HomeTeleportEvent;
import me.sharkz.ultrahomes.teleportations.TeleportationManager;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author Roch Blondiaux
 */
public class TeleportationTask extends BukkitRunnable {

    private final TeleportationManager manager;
    private final int cancelRange;

    public TeleportationTask(TeleportationManager manager, int cancelRange) {
        this.manager = manager;
        this.cancelRange = cancelRange;
    }

    @Override
    public void run() {
        manager.getTeleportation()
                .stream()
                .filter(teleportation -> teleportation.getStartDate() + teleportation.getDelay() * 1000L >= System.currentTimeMillis())
                .forEach(teleportation -> {
                    // TODO: Play particles, sounds, title, actionbar & message
                    if (teleportation.isCancelOnMove()
                            && teleportation.getFrom().distance(teleportation.getPlayer().getLocation()) >= cancelRange) {
                        // TODO: send cancel message, title, actionbar
                        manager.cancelTeleportation(teleportation);
                    }
                });

        manager.getTeleportation()
                .stream()
                .filter(teleportation -> teleportation.getStartDate() + teleportation.getDelay() * 1000L <= System.currentTimeMillis())
                .forEach(teleportation -> {
                    HomeTeleportEvent event = new HomeTeleportEvent(teleportation.getHome(), teleportation.getPlayer(), teleportation, teleportation.getPlayer().getLocation(), teleportation.getHome().getLocation());
                    Bukkit.getPluginManager().callEvent(event);
                    if (event.isCancelled()) return;

                    teleportation.getPlayer().teleport(teleportation.getHome().getLocation());
                    // TODO: Play particles, sounds, title, actionbar & message
                    manager.cancelTeleportation(teleportation);
                });
    }
}
